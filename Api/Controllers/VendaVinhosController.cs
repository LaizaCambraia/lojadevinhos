﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Interfaces;
using Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/vendavinhos")]
    public class VendaVinhosController : Controller
    {
        private readonly IVendaVinhosAppService _vendaVinhosAppService;

        public VendaVinhosController(IVendaVinhosAppService vendaVinhosAppService)
        {
            _vendaVinhosAppService = vendaVinhosAppService;
        }

        /// <summary>
        /// # 1 - Liste os clientes ordenados pelo maior valor total em compras.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(RetornoModel), (int)HttpStatusCode.OK)]
        [Route("obter/clientes")]
        public IActionResult ObterClientes()
        {
            return Ok(_vendaVinhosAppService.ObterClientes());
        }

        /// <summary>
        /// # 2 - Mostre o cliente com maior compra única no último ano.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(RetornoModel), (int)HttpStatusCode.OK)]
        [Route("obter/cliente/{ano}")]
        public IActionResult ObterCliente(int ano)
        {
            return Ok(_vendaVinhosAppService.ObterCliente(ano));
        }

        /// <summary>
        /// # 3 - Liste os clientes mais fiéis.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(RetornoModel), (int)HttpStatusCode.OK)]
        [Route("obter/clientes/fieis")]
        public IActionResult ObterClientesFieis()
        {
            return Ok(_vendaVinhosAppService.ObterClientesFieis());
        }

        /// <summary>
        /// # 4 - Recomende um vinho para um determinado cliente a partir do histórico de compras.
        /// </summary>
        /// <returns></returns> 
        [HttpGet]
        [ProducesResponseType(typeof(RetornoModel), (int)HttpStatusCode.OK)]
        [Route("recomendar-vinho/{cpf}")]
        public IActionResult RecomendarVinho(string cpf)
        {
            return Ok(_vendaVinhosAppService.RecomendarVinho(cpf));
        }
    }
}