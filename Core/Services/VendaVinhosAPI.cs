﻿using Core.Entities;
using Core.Interfaces.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Core.Services
{
    public class VendaVinhosAPI : IVendaVinhosAPI
    {
        private string EndpointClientes = "http://www.mocky.io/v2/598b16291100004705515ec5";
        private string EndpointCompras = "http://www.mocky.io/v2/598b16861100004905515ec7";

        public List<Cliente> ObterClientes()
        {
            var request = new RestRequest(Method.GET);

            var client = new RestClient(EndpointClientes);

            var response = client.Execute(request);
            var data = new List<Cliente>();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                data = JsonConvert.DeserializeObject<List<Cliente>>(response.Content);
            }

            var retorno = data == null ? new List<Cliente>() : data;

            return retorno;
        }

        public List<Compra> ObterCompras()
        {
            var request = new RestRequest(Method.GET);

            var client = new RestClient(EndpointCompras);

            var response = client.Execute(request);
            var data = new List<Compra>();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                data = JsonConvert.DeserializeObject<List<Compra>>(response.Content);
            }

            var retorno = data == null ? new List<Compra>() : data;

            return retorno;
        }
    }
}
