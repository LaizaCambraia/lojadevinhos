﻿using Core.Entities;
using Core.Interfaces.Services;
using Core.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Core.Services
{
    public class VendaVinhosService : IVendaVinhosService
    {
        private readonly IVendaVinhosAPI _vendaVinhosAPI;

        public VendaVinhosService(IVendaVinhosAPI vendaVinhosAPI)
        {
            _vendaVinhosAPI = vendaVinhosAPI;
        }

        public RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientes()
        {
            List<ClienteViewModel> lstClienteViewModel = new List<ClienteViewModel>();

            RetornoModel<List<ClienteViewModel>, HttpStatusCode> retorno = new RetornoModel<List<ClienteViewModel>, HttpStatusCode>();
            retorno.Sucesso = true;
            retorno.Tipo = HttpStatusCode.OK;
            retorno.Mensagem = "OK";

            try
            {
                List<Cliente> lstClientes = _vendaVinhosAPI.ObterClientes();
                List<Compra> lstCompras =  _vendaVinhosAPI.ObterCompras().OrderByDescending(c => c.ValorTotal).ToList();
  
                foreach (var iten in lstCompras)
                {
                    string cpf = FormataCPF(iten.Cliente);
                    if (!lstClienteViewModel.Select(x => x.CPF).Contains(cpf))
                    {
                        Cliente dadosCliente = lstClientes.Where(x => x.CPF.Replace("-", ".") == cpf).FirstOrDefault();

                        if (dadosCliente != null)
                        {
                            ClienteViewModel cliente = new ClienteViewModel()
                            {
                                Id = dadosCliente.Id,
                                CPF = dadosCliente.CPF.Replace("-", "."),
                                Nome = dadosCliente.Nome
                            };

                            lstClienteViewModel.Add(cliente);
                        }
                    }
                }

                retorno.Retorno = lstClienteViewModel;
                return retorno;

            }
            catch (Exception ex)
            {
                retorno.Sucesso = false;
                retorno.Tipo = HttpStatusCode.InternalServerError;
                retorno.Mensagem = "Ocorreu um erro interno";
                return retorno;
            }
        }


        public RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientesFieis()
        {
            List<ClienteViewModel> lstClienteViewModel = new List<ClienteViewModel>();

            RetornoModel<List<ClienteViewModel>, HttpStatusCode> retorno = new RetornoModel<List<ClienteViewModel>, HttpStatusCode>();
            retorno.Sucesso = true;
            retorno.Tipo = HttpStatusCode.OK;
            retorno.Mensagem = "OK";

            try
            {
                List<Cliente> lstClientes = _vendaVinhosAPI.ObterClientes();
                List<Compra> lstCompras = _vendaVinhosAPI.ObterCompras();

                IDictionary<string, int> dicClientes = new Dictionary<string, int>() { };

                foreach (var iten in lstCompras)
                {
                    int count = lstCompras.Where(x => x.Cliente == iten.Cliente).Count();
                    string cpf = FormataCPF(iten.Cliente);

                    if (!dicClientes.Select(x => x.Key).Contains(cpf))
                        dicClientes.Add(cpf, count);
                }

                foreach (var iten in dicClientes.OrderByDescending(x=>x.Value).Take(3))
                {
                    Cliente dadosCliente = lstClientes.Where(x => x.CPF.Replace("-", ".") == iten.Key).FirstOrDefault();

                    if (dadosCliente != null)
                    {
                        ClienteViewModel cliente = new ClienteViewModel()
                        {
                            Id = dadosCliente.Id,
                            CPF = dadosCliente.CPF.Replace("-", "."),
                            Nome = dadosCliente.Nome
                        };

                        lstClienteViewModel.Add(cliente);
                    }
                }

                retorno.Retorno = lstClienteViewModel;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.Sucesso = false;
                retorno.Tipo = HttpStatusCode.InternalServerError;
                retorno.Mensagem = "Ocorreu um erro interno";
                return retorno;
            }
        }


        public RetornoModel<ClienteViewModel, HttpStatusCode>  ObterCliente(int ano)
        {
            RetornoModel<ClienteViewModel, HttpStatusCode> retorno = new RetornoModel<ClienteViewModel, HttpStatusCode>();
            retorno.Sucesso = true;
            retorno.Tipo = HttpStatusCode.OK;
            retorno.Mensagem = "OK";

            try 
            {
                List<Compra> lstCompras = _vendaVinhosAPI.ObterCompras().Where(x => Convert.ToDateTime(x.Data).Year == ano).ToList();

                decimal maiorValor = 0;
                Compra compra = new Compra();

                foreach (var iten in lstCompras)
                {
                    if (iten.ValorTotal > maiorValor)
                    {
                        maiorValor = iten.ValorTotal;
                        compra = iten;
                    }
                }

                string cpf = FormataCPF(compra.Cliente);
                Cliente cliente = _vendaVinhosAPI.ObterClientes().Where(x => x.CPF.Replace("-", ".") == cpf).FirstOrDefault();

                ClienteViewModel clienteViewModel = new ClienteViewModel();
                if (cliente != null)
                {
                    clienteViewModel.CPF = cliente.CPF;
                    clienteViewModel.Nome = cliente.Nome;
                    clienteViewModel.Id = cliente.Id;
                }

                retorno.Retorno = clienteViewModel;
                return retorno;
            } 
            catch (Exception ex)
            {
                retorno.Sucesso = false;
                retorno.Tipo = HttpStatusCode.InternalServerError;
                retorno.Mensagem = "Ocorreu um erro interno";
                return retorno;
            }
        }

        public RetornoModel<VinhoViewModel, HttpStatusCode>  RecomendarVinho(string cpf)
        {
            RetornoModel<VinhoViewModel, HttpStatusCode> retorno = new RetornoModel<VinhoViewModel, HttpStatusCode>();
            retorno.Sucesso = true;
            retorno.Tipo = HttpStatusCode.OK;
            retorno.Mensagem = "OK";

            try
            {
                List<Compra> lstCompras = _vendaVinhosAPI.ObterCompras().Where(x => FormataCPF(x.Cliente) == cpf.Replace("-", ".")).ToList();

                IDictionary<string, int> lstVinho = new Dictionary<string, int>() { };
                Vinho vinho = new Vinho();
                int vinhoMaisComprado = 0;

                foreach (var iten in lstCompras)
                {
                    foreach (var v in iten.Vinhos)
                    {
                        int count = iten.Vinhos.Where(x => x.Produto == v.Produto).Count();

                        if (!lstVinho.Select(x => x.Key).Contains(v.Produto))
                            lstVinho.Add(v.Produto, count);
                        else
                            lstVinho[v.Produto] = lstVinho[v.Produto] + 1;

                        if (lstVinho[v.Produto] > vinhoMaisComprado)
                        {
                            vinho = v;
                            vinhoMaisComprado = lstVinho[v.Produto];
                        }
                    }
                }

                VinhoViewModel vinhoViewModel = new VinhoViewModel()
                {
                    Produto = vinho.Produto,
                    Categoria = vinho.Categoria,
                    Safra = vinho.Safra,
                    Variedade = vinho.Variedade,
                    Pais = vinho.Pais,
                    Preco = vinho.Preco
                };

                retorno.Retorno = vinhoViewModel;
                return retorno;
            }
            catch (Exception ex)
            {
                retorno.Sucesso = false;
                retorno.Tipo = HttpStatusCode.InternalServerError;
                retorno.Mensagem = "Ocorreu um erro interno";
                return retorno;
            }
        }

        public string FormataCPF(string cpf) 
        {
            //Ajuste realizado pois o CPF que consta na lista de compras tem um 0 a mais no inicio
            if (cpf.Length == 15)
                cpf = cpf.Remove(0, 1);

            return cpf;
        }
    }
}
