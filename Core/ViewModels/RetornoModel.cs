﻿namespace Core.ViewModels
{
    public class RetornoModel<T> : RetornoModel
    {
        public T Retorno { get; set; }
    }

    public class RetornoModel<T, TExecao> : RetornoModel<T>
    {
        public TExecao Tipo { get; set; }
    }

    public class RetornoModel
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
    }
}
