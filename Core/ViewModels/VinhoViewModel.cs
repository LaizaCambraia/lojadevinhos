﻿using Newtonsoft.Json;

namespace Core.ViewModels
{
    public class VinhoViewModel
    {
        public string Produto { get; set; }
        public string Variedade { get; set; }
        public string Pais { get; set; }
        public string Categoria { get; set; }
        public string Safra { get; set; }
        public decimal Preco { get; set; }
    }
}
