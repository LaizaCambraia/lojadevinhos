﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Core.ViewModels
{
    public class CompraViewModel
    {
        public string Codigo { get; set; }
        public DateTime Data { get; set; }
        public string Cliente { get; set; }
        public List<VinhoViewModel> Vinhos { get; set; }
        public decimal ValorTotal { get; set; }
    }
}