﻿using Newtonsoft.Json;
using System;

namespace Core.Entities
{
    public class Cliente
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("nome")]
        public string Nome { get; set; }
        [JsonProperty("cpf")]
        public string CPF { get; set; }
    }
}
