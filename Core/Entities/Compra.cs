﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Core.Entities
{
    public class Compra
    {
        [JsonProperty("codigo")]
        public string Codigo { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        [JsonProperty("cliente")]
        public string Cliente { get; set; }
        [JsonProperty("itens")]
        public List<Vinho> Vinhos { get; set; }
        [JsonProperty("ValorTotal")]
        public decimal ValorTotal { get; set; }
    }
}