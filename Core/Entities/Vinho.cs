﻿using Newtonsoft.Json;

namespace Core.Entities
{
    public class Vinho
    {
        [JsonProperty("produto")]
        public string Produto { get; set; }
        [JsonProperty("variedade")]
        public string Variedade { get; set; }
        [JsonProperty("pais")]
        public string Pais { get; set; }
        [JsonProperty("categoria")]
        public string Categoria { get; set; }
        [JsonProperty("safra")]
        public string Safra { get; set; }
        [JsonProperty("preco")]
        public decimal Preco { get; set; }
    }
}
