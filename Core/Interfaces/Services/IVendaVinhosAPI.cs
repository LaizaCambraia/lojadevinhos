﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Interfaces.Services
{
    public interface IVendaVinhosAPI
    {
        List<Cliente> ObterClientes();
        List<Compra> ObterCompras();
    }
}
