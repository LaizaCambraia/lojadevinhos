﻿using Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;

namespace Core.Interfaces.Services
{
    public interface IVendaVinhosService
    {
        RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientes();
        RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientesFieis();
        RetornoModel<ClienteViewModel, HttpStatusCode>   ObterCliente(int ano);
        RetornoModel<VinhoViewModel, HttpStatusCode>   RecomendarVinho(string cpf);
    }
}
