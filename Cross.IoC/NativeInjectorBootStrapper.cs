﻿using App.Interfaces;
using App.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Core.Interfaces.Services;
using Core.Services;

namespace Cross.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IVendaVinhosAppService, VendaVinhosAppService>();
            services.AddScoped<IVendaVinhosService, VendaVinhosService>();
            services.AddScoped<IVendaVinhosAPI, VendaVinhosAPI>();
        }
    }
}
