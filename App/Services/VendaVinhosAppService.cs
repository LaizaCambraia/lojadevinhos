﻿using App.Interfaces;
using Core.Interfaces.Services;
using Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace App.Services
{
    public class VendaVinhosAppService : IVendaVinhosAppService
    {
        private readonly IVendaVinhosService _vendaVinhosService;

        public VendaVinhosAppService(IVendaVinhosService vendaVinhosService)
        {
            _vendaVinhosService = vendaVinhosService;
        }

        public RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientes()
        {
            return _vendaVinhosService.ObterClientes();
        }

        public RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientesFieis()
        {
            return _vendaVinhosService.ObterClientesFieis();
        }

        public RetornoModel<ClienteViewModel, HttpStatusCode>  ObterCliente(int ano)
        {
            return _vendaVinhosService.ObterCliente(ano);
        }

        public RetornoModel<VinhoViewModel, HttpStatusCode>  RecomendarVinho(string cpf) 
        {
            return _vendaVinhosService.RecomendarVinho(cpf);
        }
    }
}
