﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Core.ViewModels;

namespace App.Interfaces
{
    public interface IVendaVinhosAppService
    {
        RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientes();
        RetornoModel<List<ClienteViewModel>, HttpStatusCode> ObterClientesFieis();
        RetornoModel<ClienteViewModel, HttpStatusCode> ObterCliente(int ano);
        RetornoModel<VinhoViewModel, HttpStatusCode> RecomendarVinho(string cpf);
    }
}
